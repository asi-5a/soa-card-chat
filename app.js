const bodyParser = require('body-parser');
var app = require('express')();
app.use(bodyParser.urlencoded({ extended: true }));
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var ent = require('ent');// Permet de bloquer les caractères HTML

io.use((socket, next) => {
    let token = socket.handshake.query.token;
    if (isValid(token)) {
        return next();
    }
    return next(new Error('authentication error'));
});

var mq = require('./ActiveMq');

mq.init();

server.listen(9001);

const roomPrefix = 'battleRoom_';

const listRoomsMatchMaking = new Map();
const listRoomsInGame = new Map();

const userConnected = [];

app.post('/save_chat', function (req, res) {
    console.log('Got body:', req.body);
    mq.sendMessage(req.body);
    res.sendStatus(200);
});

io.on('connection', function (socket, pseudo) {
    let token = socket.handshake.query.token;
    pseudo = ent.encode(token);
    const exists = userConnected.findIndex(u => u.username === pseudo);
    if (exists > -1) {
        userConnected.splice(exists, 1);
    }
    userConnected.push({
        username: pseudo,
        id: socket.id
    });
    socket.username = pseudo;
    console.log('new entry in tab', userConnected);
    console.log('found new client: ' + pseudo);
    io.emit('nouveau_client', userConnected);


    // Dès qu'on reçoit un message, on récupère le pseudo de son auteur et on le transmet aux autres personnes
    socket.on('message', function (payload) {
        const message = ent.encode(payload.message);
        const sender = userConnected.find(u => u.id === socket.id);
        const payloadSend = {
            message: message,
            pseudo: sender.username,
            senderSocketId: socket.id
        };
        io.to(payload.id).emit('message', payloadSend);
    });

    socket.on('matchmaking', function (card) {
        var thisGameId;
        var room;
        if (listRoomsMatchMaking.size > 0) {
            var next = listRoomsMatchMaking.values().next();
            room = next.value;
            thisGameId = room.roomId;
            if (!room.j2.hasOwnProperty('username') && socket.username !== room.j1.username) {
                room.j2.username = socket.username;
                room.j2.card = JSON.parse(card);
            }
        } else {
            thisGameId = createRoom(socket, card);
            room = listRoomsMatchMaking.get(thisGameId);
        }

        socket.join(thisGameId);

        if (room.j2.hasOwnProperty('username')) {
            room.firstRound = (Math.floor(Math.random() * 2) + 1);
            io.to(thisGameId).emit('NewGame', room);
            listRoomsInGame.set(thisGameId, room);
            listRoomsMatchMaking.delete(thisGameId);
        }
    });
});

function createRoom(socket, card) {
    var thisGameId = roomPrefix + ((Math.random() * 100000) | 0);
    listRoomsMatchMaking.set(thisGameId, {
        "roomId": thisGameId,
        "j1": {
            username: socket.username,
            card: JSON.parse(card)
        },
        "j2": {},
    });
    return thisGameId;
}

function isValid(token) { // validation toujours OK pour le moment
    if (!token || token === "undefined") {
        return false;
    } else {
        return true;
    }
}